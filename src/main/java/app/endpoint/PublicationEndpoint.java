package app.endpoint;

import app.action.ChannelAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import request.DeleteDtoRequest;
import request.PublicationDtoRequest;
import response.DeleteDtoResponse;
import response.PublicationDtoResponse;

import javax.validation.Valid;


@RestController
@RequestMapping("/publication")
public class PublicationEndpoint {

    private final ChannelAction channelAction;

    @Autowired
    public PublicationEndpoint(ChannelAction channelAction) {
        this.channelAction = channelAction;
    }


    @PostMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public PublicationDtoResponse publicationInChannel(@PathVariable("id") int id, @Valid @RequestBody PublicationDtoRequest message) {
        if (id <= 0 ) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Incorrect parameters");
        }
        return channelAction.publicationInChannel(message);
    }

    @DeleteMapping (value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public DeleteDtoResponse deleteOfChannel(@PathVariable("id") int id,@Valid @RequestBody DeleteDtoRequest message) {
        if (id <= 0 ) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Incorrect parameters");
        }
        return channelAction.deleteOfChannel(message);
    }

}
