package app.endpoint;

import app.action.RecordingAction;
import app.validator.RecordingValidator;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;
import org.springframework.web.server.ResponseStatusException;
import request.RecordingDtoRequest;
import response.RecordingDtoResponse;

import javax.validation.Valid;
import java.util.Optional;


@RestController
@RequestMapping("/recording")
public class RecordingEndpoint {


    private final RecordingAction userAction;

    public RecordingEndpoint(RecordingAction userAction) {
        this.userAction = userAction;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RecordingDtoResponse recording(@Valid @RequestBody RecordingDtoRequest recordingDtoRequest) {
        userAction.recordingSong(recordingDtoRequest);
        return new RecordingDtoResponse(1);
    }






}
