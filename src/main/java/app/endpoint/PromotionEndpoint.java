package app.endpoint;

import app.action.PromotionAction;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import request.DeletePromotionDtoRequest;
import request.PromotionDtoRequest;
import response.DeletePromotionDtoResponse;
import response.PromotionDtoResponse;

import javax.validation.Valid;

@RestController
@RequestMapping("/promotion")
public class PromotionEndpoint {

    private final PromotionAction promotionAction;

    public PromotionEndpoint(PromotionAction promotionAction) {
        this.promotionAction = promotionAction;
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public PromotionDtoResponse Promotion(@PathVariable("id") int id, @Valid @RequestBody PromotionDtoRequest promotionDtoRequest) {
        if (id <= 0 ) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Incorrect parameters");
        }
        return promotionAction.statePromotion(promotionDtoRequest);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public DeletePromotionDtoResponse deletePromotion(@PathVariable("id") int id, @Valid @RequestBody DeletePromotionDtoRequest deletePromotionDtoRequest) {
        if (id <= 0 ) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Incorrect parameters");
        }
        return promotionAction.deletePromotion(deletePromotionDtoRequest);
    }

}
