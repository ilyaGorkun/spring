package app.validator;


import org.springframework.stereotype.Component;
import request.RecordingDtoRequest;

import java.lang.reflect.Field;
import java.util.Optional;

@Component
public class RecordingValidator {

    public Optional<RecordingDtoRequest> validator(final RecordingDtoRequest recordingDtoRequest) {
        if (recordingDtoRequest.getAlbumTitle() == null || recordingDtoRequest.getAlbumTitle().equals("")) {
            return Optional.empty();
        }

        for (Field field: recordingDtoRequest.getClass().getDeclaredFields()) {
            try {
                field.setAccessible(true);
                if(field.get(recordingDtoRequest) == null) {
                    return Optional.empty();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return Optional.of(recordingDtoRequest);
    }

}
