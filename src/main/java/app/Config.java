package app;

import IServices.DataStorage;
import IServices.PromotionService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import services.*;

@Configuration
@ComponentScan
public class Config {

    @Bean
    public DataStorage recordingStorageAudio() {
        return new AudioRecordingStorage();
    }

    @Bean
    public PromotionService promotionService() {
        return new PromotionServiceImpl();
    }

    @Bean
    public ItunesChanel getItunesChanel() {
        return new ItunesChanel();
    }

    @Bean
    public YandexMusicChannel getYandexMusicChanel() {
        return new YandexMusicChannel();
    }

    @Bean
    public YoutubeMusicChannel getYouTubeChanel() {
        return new YoutubeMusicChannel();
    }

    @Bean
    public AudioRecordingStorage getAudioRecordingStorage() {
        return new AudioRecordingStorage();
    }

    @Bean
    public VideoRecordingStorage getVideoRecordingStorage() {
        return new VideoRecordingStorage();
    }


}
