package app.action;

import IServices.DataStorage;
import org.springframework.stereotype.Service;
import request.RecordingDtoRequest;
import response.*;
import services.AudioRecordingStorage;
import structure.Recording;


@Service
public class RecordingAction {


    private final DataStorage audioRecordingStorage;

    public RecordingAction(AudioRecordingStorage audioRecordingStorage) {
        this.audioRecordingStorage = audioRecordingStorage;
    }

    public RecordingDtoResponse recordingSong(RecordingDtoRequest recordingDtoRequest) {
        Recording recording = new Recording(recordingDtoRequest.getArtist(),recordingDtoRequest.getSongTitle(),
                                            recordingDtoRequest.getAlbumTitle(), recordingDtoRequest.getYearOfIssue(),
                                            recordingDtoRequest.getAlbumCoverLink(),recordingDtoRequest.getGenre(),
                                            recordingDtoRequest.getDuration(),recordingDtoRequest.getLinkToAudioFile(),
                                            recordingDtoRequest.getLinkToVideoFile());
        audioRecordingStorage.save(recording.getLinkToAudioFile());
        return new RecordingDtoResponse(1);
    }







}
