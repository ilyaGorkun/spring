package app.action;

import IServices.DataStorage;
import IServices.PublishingChannels;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import request.DeleteDtoRequest;
import request.PublicationDtoRequest;
import response.DeleteDtoResponse;
import response.PublicationDtoResponse;
import services.AudioRecordingStorage;
import services.ItunesChanel;
import services.YandexMusicChannel;
import services.YoutubeMusicChannel;

import java.time.ZonedDateTime;

@Service
public class ChannelAction {

    private final PublishingChannels itunesChanel;
    private final PublishingChannels yandexMusicChannel;
    private final PublishingChannels youTubeMusicChannels;
    private final DataStorage audioRecordingStorage;

    @Autowired
    public ChannelAction(ItunesChanel itunesChanel, YandexMusicChannel yandexMusicChannel, YoutubeMusicChannel youTubeMusicChannels, AudioRecordingStorage audioRecordingStorage) {
        this.itunesChanel = itunesChanel;
        this.yandexMusicChannel = yandexMusicChannel;
        this.youTubeMusicChannels = youTubeMusicChannels;
        this.audioRecordingStorage = audioRecordingStorage;
    }

    public PublicationDtoResponse publicationInChannel(PublicationDtoRequest publicationDtoRequest) {

        if (publicationDtoRequest.getChannelName().equals("Itunes")) {
            return publicationItunes(publicationDtoRequest);
        }

        if (publicationDtoRequest.getChannelName().equals("YandexMusic")) {
            return publicationYandexMusic(publicationDtoRequest);
        }

        if (publicationDtoRequest.getChannelName().equals("YouTube")) {
            return publicationYouTubeMusic(publicationDtoRequest);
        }

        throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, "Incorrect parameters");
    }


    public DeleteDtoResponse deleteOfChannel(DeleteDtoRequest deleteDtoRequest) {
        if (deleteDtoRequest.getChannelName().equals("Itunes")) {
            return deleteItunes(deleteDtoRequest);
        }

        if (deleteDtoRequest.getChannelName().equals("YandexMusic")) {
            return deleteYandexMusic(deleteDtoRequest);
        }

        if (deleteDtoRequest.getChannelName().equals("YouTube")) {
            return deleteYouTubeMusic(deleteDtoRequest);
        }

        throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, "Incorrect parameters");
    }

    private PublicationDtoResponse publicationItunes(PublicationDtoRequest publicationDtoRequest) {
        itunesChanel.publish(audioRecordingStorage.getRecording(publicationDtoRequest.getId()), publicationDtoRequest.getPostTime() !=null ? publicationDtoRequest.getPostTime() : ZonedDateTime.now());
        return new PublicationDtoResponse("Song published in iTunes");
    }

    private PublicationDtoResponse publicationYandexMusic(PublicationDtoRequest publicationDtoRequest) {
        yandexMusicChannel.publish(audioRecordingStorage.getRecording(publicationDtoRequest.getId()), publicationDtoRequest.getPostTime() !=null ? publicationDtoRequest.getPostTime() : ZonedDateTime.now());
        return new PublicationDtoResponse("Song published in YandexMusic");
    }

    private PublicationDtoResponse publicationYouTubeMusic(PublicationDtoRequest publicationDtoRequest) {
        youTubeMusicChannels.publish(audioRecordingStorage.getRecording(publicationDtoRequest.getId()), publicationDtoRequest.getPostTime() !=null ? publicationDtoRequest.getPostTime() : ZonedDateTime.now());
        return new PublicationDtoResponse("Song published in YouTube");
    }

    private DeleteDtoResponse deleteItunes(DeleteDtoRequest deleteRecordingDtoRequest) {
        itunesChanel.delete(audioRecordingStorage.getRecording(deleteRecordingDtoRequest.getId()));
        return new DeleteDtoResponse("Song removed from channel Itunes");
    }

    private DeleteDtoResponse deleteYandexMusic(DeleteDtoRequest deleteRecordingDtoRequest) {
        yandexMusicChannel.delete(audioRecordingStorage.getRecording(deleteRecordingDtoRequest.getId()));
        return new DeleteDtoResponse("Song removed from channel YandexMusic");
    }

    private DeleteDtoResponse deleteYouTubeMusic(DeleteDtoRequest deleteRecordingDtoRequest) {
        youTubeMusicChannels.delete(audioRecordingStorage.getRecording(deleteRecordingDtoRequest.getId()));
        return new DeleteDtoResponse("Song removed from channel YouTube");
    }

}
