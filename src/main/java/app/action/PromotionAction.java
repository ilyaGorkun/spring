package app.action;

import IServices.DataStorage;
import IServices.PromotionService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import request.DeletePromotionDtoRequest;
import request.PromotionDtoRequest;
import response.DeletePromotionDtoResponse;
import response.PromotionDtoResponse;
import services.AudioRecordingStorage;

import java.time.ZonedDateTime;

@Service
public class PromotionAction {

    private final DataStorage audioRecordingStorage;
    private final PromotionService promotionService;

    public PromotionAction(AudioRecordingStorage audioRecordingStorage, PromotionService promotionService) {
        this.audioRecordingStorage = audioRecordingStorage;
        this.promotionService = promotionService;
    }

    public PromotionDtoResponse statePromotion(PromotionDtoRequest promotionDtoRequest) {
        if (promotionDtoRequest.getState().equals("start")) {
            return startPromotion(promotionDtoRequest);
        }

        if (promotionDtoRequest.getState().equals("stop")) {
            return stopPromotion(promotionDtoRequest);
        }

        throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, "Incorrect parameters");
    }

    private PromotionDtoResponse startPromotion(PromotionDtoRequest startPromotionDtoRequest) {
        ZonedDateTime zonedDateTime = startPromotionDtoRequest.getZonedDateTime() != null ? startPromotionDtoRequest.getZonedDateTime() : ZonedDateTime.now();
        promotionService.createCampaign(audioRecordingStorage.getRecording(startPromotionDtoRequest.getId()), zonedDateTime);
        return new PromotionDtoResponse("Advertising campaign began");
    }

    private PromotionDtoResponse stopPromotion(PromotionDtoRequest stopPromotionDtoRequest) {
        ZonedDateTime zonedDateTime = stopPromotionDtoRequest.getZonedDateTime() != null ? stopPromotionDtoRequest.getZonedDateTime() : ZonedDateTime.now();
        promotionService.stopCampaign(audioRecordingStorage.getRecording(stopPromotionDtoRequest.getId()), zonedDateTime);
        return new PromotionDtoResponse("Advertising campaign stopped");
    }

    public DeletePromotionDtoResponse deletePromotion(DeletePromotionDtoRequest deletePromotionDtoRequest) {
        ZonedDateTime zonedDateTime = deletePromotionDtoRequest.getCampaignDeleteDate() != null ? deletePromotionDtoRequest.getCampaignDeleteDate() : ZonedDateTime.now();
        promotionService.deleteCampaign(audioRecordingStorage.getRecording(deletePromotionDtoRequest.getId()),zonedDateTime);
        return new DeletePromotionDtoResponse("Advertising campaign deleted");
    }


}
