package IServices;





import structure.Recording;

import java.time.ZonedDateTime;

public interface PromotionService {

    void createCampaign(Recording recording, ZonedDateTime campaignCreateDate);
    void stopCampaign(Recording recording, ZonedDateTime campaignStopDate);
    void deleteCampaign(Recording recording, ZonedDateTime campaignDeleteDate);

}
