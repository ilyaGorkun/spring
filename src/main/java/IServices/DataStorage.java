package IServices;

import structure.Recording;

public interface DataStorage {

    String save(String path);
    Recording getRecording(int id);

}
