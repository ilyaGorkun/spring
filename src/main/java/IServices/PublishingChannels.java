package IServices;



import structure.Recording;

import java.time.ZonedDateTime;

public interface PublishingChannels {

    void publish(Recording recording, ZonedDateTime zonedDateTime);

    void delete(Recording recording);


}
