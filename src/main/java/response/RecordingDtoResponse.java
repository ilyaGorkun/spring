package response;

import java.util.Objects;

public class RecordingDtoResponse {

    private int id;

    public RecordingDtoResponse() {
    }

    public RecordingDtoResponse(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecordingDtoResponse that = (RecordingDtoResponse) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
