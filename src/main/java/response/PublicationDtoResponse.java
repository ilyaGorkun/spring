package response;

import java.util.Objects;

public class PublicationDtoResponse {

    private String status;

    public PublicationDtoResponse(String status) {
        this.status = status;
    }

    public PublicationDtoResponse() {
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PublicationDtoResponse that = (PublicationDtoResponse) o;
        return Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status);
    }
}
