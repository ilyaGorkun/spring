package response;

import java.util.Objects;

public class PromotionDtoResponse {
    private String status;

    public PromotionDtoResponse(String status) {
        this.status = status;
    }


    public PromotionDtoResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PromotionDtoResponse that = (PromotionDtoResponse) o;
        return Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status);
    }
}
