package response;

import java.util.Objects;

public class DeleteDtoResponse {

    private String status;

    public DeleteDtoResponse(String status) {
        this.status = status;
    }

    public DeleteDtoResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeleteDtoResponse that = (DeleteDtoResponse) o;
        return Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status);
    }
}

