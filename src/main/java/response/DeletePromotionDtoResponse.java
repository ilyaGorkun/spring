package response;

import java.util.Objects;

public class DeletePromotionDtoResponse {

    private String status;

    public DeletePromotionDtoResponse(String status) {
        this.status = status;
    }

    public DeletePromotionDtoResponse() {
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeletePromotionDtoResponse that = (DeletePromotionDtoResponse) o;
        return Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status);
    }
}
