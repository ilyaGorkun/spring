package services;



import IServices.DataStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecordingDataHub {

    private final DataStorage videoRecordingStorage;
    private final DataStorage audioRecordingStorage;

    @Autowired
    public RecordingDataHub(AudioRecordingStorage audioRecordingStorage, VideoRecordingStorage videoRecordingStorage) {
        this.videoRecordingStorage = videoRecordingStorage;
        this.audioRecordingStorage = audioRecordingStorage;
    }

    public void saveVideo(String path) {
        videoRecordingStorage.save(path);
        System.out.println("Video has been saved");
    }

    public void saveMusic(String path){
        audioRecordingStorage.save(path);
        System.out.println("Music has been saved");
    }


}
