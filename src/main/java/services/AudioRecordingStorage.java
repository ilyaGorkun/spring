package services;



import IServices.DataStorage;
import org.springframework.stereotype.Component;
import structure.Recording;

@Component
public class AudioRecordingStorage implements DataStorage {
    @Override
    public String save(String path) {
        return "./music/...";
    }

    @Override
    public Recording getRecording(int id) {
        return new Recording("Led zeppelin", "Black dog","Led zeppelin 4",
                "8-11-1971","./Cover/Led.png","Rock", "5.05",
                "./audio/black_dog.mp3", "./video/black_dog.png");
    }
}
