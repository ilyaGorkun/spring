package services;

import IServices.PromotionService;
import org.springframework.stereotype.Service;
import structure.Recording;


import java.time.ZonedDateTime;

@Service
public class PromotionServiceImpl implements PromotionService {
    @Override
    public void createCampaign(Recording recording, ZonedDateTime campaignCreateDate) {
        System.out.println("For " + recording.getSongTitle() + " was created advertising campaign at  " + campaignCreateDate.toString());
    }

    @Override
    public void stopCampaign(Recording recording, ZonedDateTime campaignStopDate) {
        System.out.println("For " + recording.getSongTitle() + " was stoped advertising campaign at  " + campaignStopDate.toString());
    }

    @Override
    public void deleteCampaign(Recording recording, ZonedDateTime campaignDeleteDate) {
        System.out.println("For " + recording.getSongTitle() + " was deleted advertising campaign at  " + campaignDeleteDate.toString());
    }
}
