package services;

import IServices.PublishingChannels;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import structure.Recording;


import java.time.ZonedDateTime;

@Component
public class ItunesChanel implements PublishingChannels {


    @Override
    public void publish(Recording recording, ZonedDateTime zonedDateTime) {
        System.out.println(recording.getSongTitle() + " was published in Itunes at " + zonedDateTime.toString());
    }

    @Override
    public void delete(Recording recording) {
        System.out.println(recording.getSongTitle() + " was deleted in Itunes ");
    }
}
