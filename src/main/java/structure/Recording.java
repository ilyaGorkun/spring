package structure;
import java.util.Objects;

public class Recording {

   private String artist;
   private String songTitle;
   private String albumTitle;
   private String yearOfIssue;
   private String albumCoverLink;
   private String genre;
   private String duration;
   private String linkToAudioFile;
   private String linkToVideoFile;



    public Recording(String artist, String songTitle, String albumTitle, String yearOfIssue, String albumCoverLink, String genre, String duration, String linkToAudioFile, String linkToVideoFile) {
        this.artist = artist;
        this.songTitle = songTitle;
        this.albumTitle = albumTitle;
        this.yearOfIssue = yearOfIssue;
        this.albumCoverLink = albumCoverLink;
        this.genre = genre;
        this.duration = duration;
        this.linkToAudioFile = linkToAudioFile;
        this.linkToVideoFile = linkToVideoFile;
    }

    public Recording() {
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public void setSongTitle(String songTitle) {
        this.songTitle = songTitle;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }

    public String getYearOfIssue() {
        return yearOfIssue;
    }

    public void setYearOfIssue(String yearOfIssue) {
        this.yearOfIssue = yearOfIssue;
    }

    public String getAlbumCoverLink() {
        return albumCoverLink;
    }

    public void setAlbumCoverLink(String albumCoverLink) {
        this.albumCoverLink = albumCoverLink;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getLinkToAudioFile() {
        return linkToAudioFile;
    }

    public void setLinkToAudioFile(String linkToAudioFile) {
        this.linkToAudioFile = linkToAudioFile;
    }

    public String getLinkToVideoFile() {
        return linkToVideoFile;
    }

    public void setLinkToVideoFile(String linkToVideoFile) {
        this.linkToVideoFile = linkToVideoFile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recording recording = (Recording) o;
        return Objects.equals(artist, recording.artist) &&
                Objects.equals(songTitle, recording.songTitle) &&
                Objects.equals(albumTitle, recording.albumTitle) &&
                Objects.equals(yearOfIssue, recording.yearOfIssue) &&
                Objects.equals(albumCoverLink, recording.albumCoverLink) &&
                Objects.equals(genre, recording.genre) &&
                Objects.equals(duration, recording.duration) &&
                Objects.equals(linkToAudioFile, recording.linkToAudioFile) &&
                Objects.equals(linkToVideoFile, recording.linkToVideoFile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(artist, songTitle, albumTitle, yearOfIssue, albumCoverLink, genre, duration, linkToAudioFile, linkToVideoFile);
    }
}
