package request;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class RecordingDtoRequest {

    @NotNull
    private String artist;
    @NotNull
    private String songTitle;
    @NotNull
    private String albumTitle;
    @NotNull
    private String yearOfIssue;
    @NotNull
    private String albumCoverLink;
    @NotNull
    private String genre;
    @NotNull
    private String duration;
    @NotNull
    private String linkToAudioFile;
    @NotNull
    private String linkToVideoFile;

    public RecordingDtoRequest() {
    }

    public RecordingDtoRequest(String artist, String songTitle, String albumTitle, String yearOfIssue, String albumCoverLink, String genre, String duration, String linkToAudioFile, String linkToVideoFile) {
        this.artist = artist;
        this.songTitle = songTitle;
        this.albumTitle = albumTitle;
        this.yearOfIssue = yearOfIssue;
        this.albumCoverLink = albumCoverLink;
        this.genre = genre;
        this.duration = duration;
        this.linkToAudioFile = linkToAudioFile;
        this.linkToVideoFile = linkToVideoFile;
    }

    public String getArtist() {
        return artist;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public String getYearOfIssue() {
        return yearOfIssue;
    }

    public String getAlbumCoverLink() {
        return albumCoverLink;
    }

    public String getGenre() {
        return genre;
    }

    public String getDuration() {
        return duration;
    }

    public String getLinkToAudioFile() {
        return linkToAudioFile;
    }

    public String getLinkToVideoFile() {
        return linkToVideoFile;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setSongTitle(String songTitle) {
        this.songTitle = songTitle;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }

    public void setYearOfIssue(String yearOfIssue) {
        this.yearOfIssue = yearOfIssue;
    }

    public void setAlbumCoverLink(String albumCoverLink) {
        this.albumCoverLink = albumCoverLink;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public void setLinkToAudioFile(String linkToAudioFile) {
        this.linkToAudioFile = linkToAudioFile;
    }

    public void setLinkToVideoFile(String linkToVideoFile) {
        this.linkToVideoFile = linkToVideoFile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecordingDtoRequest that = (RecordingDtoRequest) o;
        return Objects.equals(artist, that.artist) &&
                Objects.equals(songTitle, that.songTitle) &&
                Objects.equals(albumTitle, that.albumTitle) &&
                Objects.equals(yearOfIssue, that.yearOfIssue) &&
                Objects.equals(albumCoverLink, that.albumCoverLink) &&
                Objects.equals(genre, that.genre) &&
                Objects.equals(duration, that.duration) &&
                Objects.equals(linkToAudioFile, that.linkToAudioFile) &&
                Objects.equals(linkToVideoFile, that.linkToVideoFile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(artist, songTitle, albumTitle, yearOfIssue, albumCoverLink, genre, duration, linkToAudioFile, linkToVideoFile);
    }
}
