package request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.Objects;

public class DeletePromotionDtoRequest {

    @NotNull
    @Min(value = 1)
    private int id;

    private ZonedDateTime campaignDeleteDate;

    public DeletePromotionDtoRequest() {
    }

    public DeletePromotionDtoRequest(@NotNull @Min(value = 1) int id, ZonedDateTime campaignDeleteDate) {
        this.id = id;
        this.campaignDeleteDate = campaignDeleteDate;
    }

    public DeletePromotionDtoRequest(@NotNull @Min(value = 1) int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ZonedDateTime getCampaignDeleteDate() {
        return campaignDeleteDate;
    }

    public void setCampaignDeleteDate(ZonedDateTime campaignDeleteDate) {
        this.campaignDeleteDate = campaignDeleteDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeletePromotionDtoRequest that = (DeletePromotionDtoRequest) o;
        return id == that.id &&
                Objects.equals(campaignDeleteDate, that.campaignDeleteDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, campaignDeleteDate);
    }
}
