package request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.Objects;

public class PromotionDtoRequest {

    @NotNull
    @Min(value = 1)
    private int id;

    @NotNull
    private String state;
    private ZonedDateTime zonedDateTime;

    public PromotionDtoRequest() {
    }

    public PromotionDtoRequest(@NotNull @Min(value = 1) int id, @NotNull @Min(value = 3) String state, ZonedDateTime zonedDateTime) {
        this.id = id;
        this.state = state;
        this.zonedDateTime = zonedDateTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ZonedDateTime getZonedDateTime() {
        return zonedDateTime;
    }

    public void setZonedDateTime(ZonedDateTime zonedDateTime) {
        this.zonedDateTime = zonedDateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PromotionDtoRequest that = (PromotionDtoRequest) o;
        return id == that.id &&
                Objects.equals(state, that.state) &&
                Objects.equals(zonedDateTime, that.zonedDateTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, state, zonedDateTime);
    }
}
