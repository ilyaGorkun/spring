package request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

public class DeleteDtoRequest {

    @NotNull
    @Min(value = 1)
    private int id;

    @NotNull
    @Size(min = 3)
    private String channelName;


    public DeleteDtoRequest() {
    }

    public DeleteDtoRequest(@NotNull @Min(value = 1) int id, @NotNull String channelName) {
        this.id = id;
        this.channelName = channelName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeleteDtoRequest that = (DeleteDtoRequest) o;
        return id == that.id &&
                Objects.equals(channelName, that.channelName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, channelName);
    }
}
