package request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;
import java.util.Objects;

public class PublicationDtoRequest {

    @NotNull
    @Min(value = 1)
    private int id;

    @NotNull
    @Size(min = 3)
    private String channelName;

    private ZonedDateTime postTime;

    public PublicationDtoRequest() {

    }

    public PublicationDtoRequest(@NotNull @Min(value = 1) int id, @NotNull String channelName, ZonedDateTime postTime) {
        this.id = id;
        this.channelName = channelName;
        this.postTime = postTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public ZonedDateTime getPostTime() {
        return postTime;
    }

    public void setPostTime(ZonedDateTime postTime) {
        this.postTime = postTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PublicationDtoRequest that = (PublicationDtoRequest) o;
        return id == that.id &&
                Objects.equals(channelName, that.channelName) &&
                Objects.equals(postTime, that.postTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, channelName, postTime);
    }
}
