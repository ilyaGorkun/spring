package app.requestTest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;
import request.RecordingDtoRequest;
import response.RecordingDtoResponse;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class RecordingTest {

    private RestTemplate restTemplate = new RestTemplate();


    @Test
    public void testPostRecording() {
        RecordingDtoRequest recordingDtoRequest = new RecordingDtoRequest("Led zeppelin", "Black dog","Led zeppelin 4",
                "8-11-1971","./Cover/Led.png","Rock", "5.05",
                "./audio/black_dog.mp3", "./video/black_dog.png");

        RecordingDtoResponse id = restTemplate.postForObject("http://localhost:8080/recording",recordingDtoRequest, RecordingDtoResponse.class);
        assertEquals(id.getId(), 1);

    }

}
