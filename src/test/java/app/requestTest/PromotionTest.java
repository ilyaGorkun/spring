package app.requestTest;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;
import request.DeletePromotionDtoRequest;
import request.PromotionDtoRequest;
import response.DeletePromotionDtoResponse;
import response.PromotionDtoResponse;

import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class PromotionTest {

    private RestTemplate restTemplate = new RestTemplate();

    @Test
    public void startPromotionCampaign() {
        HttpEntity<PromotionDtoRequest> promotionDtoRequest = new HttpEntity<>(new PromotionDtoRequest(12, "start", ZonedDateTime.now()));

        ResponseEntity<PromotionDtoResponse> responseResponseEntity = restTemplate.exchange("http://localhost:8080/promotion/12", HttpMethod.PUT, promotionDtoRequest,PromotionDtoResponse.class);

        assertEquals(responseResponseEntity.getBody().getStatus(), "Advertising campaign began");

    }

    @Test
    public void stopPromotionCampaign() {
        HttpEntity<PromotionDtoRequest> promotionDtoRequest = new HttpEntity<>(new PromotionDtoRequest(12, "stop", ZonedDateTime.now()));

        ResponseEntity<PromotionDtoResponse> responseResponseEntity = restTemplate.exchange("http://localhost:8080/promotion/12", HttpMethod.PUT, promotionDtoRequest,PromotionDtoResponse.class);

        assertEquals(responseResponseEntity.getBody().getStatus(), "Advertising campaign stopped");

    }

    @Test
    public void deletePromotionCampaign() {
        HttpEntity<DeletePromotionDtoRequest> deletePromotionDtoRequest = new HttpEntity<>(new DeletePromotionDtoRequest(12));

        ResponseEntity<DeletePromotionDtoResponse> deleteResponseResponseEntity = restTemplate.exchange("http://localhost:8080/promotion/12", HttpMethod.DELETE, deletePromotionDtoRequest, DeletePromotionDtoResponse.class);

        assertEquals(deleteResponseResponseEntity.getBody().getStatus(), "Advertising campaign deleted");

    }




}
