package app.requestTest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;
import request.DeleteDtoRequest;
import request.PublicationDtoRequest;
import response.DeleteDtoResponse;
import response.PublicationDtoResponse;


import static org.junit.jupiter.api.Assertions.*;

import java.time.ZonedDateTime;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class PublicationTest {

    private RestTemplate restTemplate = new RestTemplate();

    @Test
    public void postPublicationInItunesChannel() {
        PublicationDtoRequest publicationDtoRequest = new PublicationDtoRequest(12, "Itunes", ZonedDateTime.now());

        PublicationDtoResponse publicationDtoResponse = restTemplate.postForObject("http://localhost:8080/publication/12", publicationDtoRequest, PublicationDtoResponse.class);
        assertEquals(publicationDtoResponse.getStatus(), "Song published in iTunes");
    }

    @Test
    public void postPublicationInYandexChannel() {
        PublicationDtoRequest publicationDtoRequest = new PublicationDtoRequest(12, "YandexMusic", ZonedDateTime.now());

        PublicationDtoResponse publicationDtoResponse = restTemplate.postForObject("http://localhost:8080/publication/12", publicationDtoRequest, PublicationDtoResponse.class);
        assertEquals(publicationDtoResponse.getStatus(), "Song published in YandexMusic");
    }

    @Test
    public void postPublicationInYouTubeChannel() {
        PublicationDtoRequest publicationDtoRequest = new PublicationDtoRequest(12, "YouTube", ZonedDateTime.now());

        PublicationDtoResponse publicationDtoResponse = restTemplate.postForObject("http://localhost:8080/publication/12", publicationDtoRequest, PublicationDtoResponse.class);
        assertEquals(publicationDtoResponse.getStatus(), "Song published in YouTube");
    }

    @Test
    public void deletePublicationOItunesChannel() {
        HttpEntity<DeleteDtoRequest> deleteDtoRequestHttpEntity = new HttpEntity<>(new DeleteDtoRequest(12, "Itunes"));

        ResponseEntity<DeleteDtoResponse> deleteDtoResponseResponseEntity = restTemplate.exchange("http://localhost:8080/publication/12", HttpMethod.DELETE, deleteDtoRequestHttpEntity, DeleteDtoResponse.class);

        assertEquals(deleteDtoResponseResponseEntity.getBody().getStatus(), "Song removed from channel Itunes");
    }

    @Test
    public void deletePublicationOYandexChannel() {
        HttpEntity<DeleteDtoRequest> deleteDtoRequestHttpEntity = new HttpEntity<>(new DeleteDtoRequest(12, "YandexMusic"));

        ResponseEntity<DeleteDtoResponse> deleteDtoResponseResponseEntity = restTemplate.exchange("http://localhost:8080/publication/12", HttpMethod.DELETE, deleteDtoRequestHttpEntity, DeleteDtoResponse.class);

        assertEquals(deleteDtoResponseResponseEntity.getBody().getStatus(), "Song removed from channel YandexMusic");
    }


    @Test
    public void deletePublicationOYouTubeChannel() {
        HttpEntity<DeleteDtoRequest> deleteDtoRequestHttpEntity = new HttpEntity<>(new DeleteDtoRequest(12, "YouTube"));

        ResponseEntity<DeleteDtoResponse> deleteDtoResponseResponseEntity = restTemplate.exchange("http://localhost:8080/publication/12", HttpMethod.DELETE, deleteDtoRequestHttpEntity, DeleteDtoResponse.class);

        assertEquals(deleteDtoResponseResponseEntity.getBody().getStatus(), "Song removed from channel YouTube");
    }
}
