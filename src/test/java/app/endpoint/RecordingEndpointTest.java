package app.endpoint;

import app.action.RecordingAction;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import request.RecordingDtoRequest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = RecordingEndpoint.class)
public class RecordingEndpointTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper = new ObjectMapper();

    @MockBean
    private RecordingAction recordingAction;

    @Test
    public void testPostRecording1() throws Exception{
        RecordingDtoRequest recordingDtoRequest = new RecordingDtoRequest("Led zeppelin", "Black dog","Led zeppelin 4",
                "8-11-1971","./Cover/Led.png","Rock", "5.05",
                "./audio/black_dog.mp3", "./video/black_dog.png");

        MvcResult result = mvc.perform(post("/recording")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(recordingDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 200);
    }

    @Test
    public void testPostRecording2() throws Exception{
        RecordingDtoRequest recordingDtoRequest = new RecordingDtoRequest("Led zeppelin", "Black dog","Led zeppelin 4",
                "8-11-1971","./Cover/Led.png",null, "5.05",
                "./audio/black_dog.mp3", "./video/black_dog.png");

        MvcResult result = mvc.perform(post("/recording")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(recordingDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 400);
    }

    @Test
    public void testPostRecordin3() throws Exception{
        RecordingDtoRequest recordingDtoRequest = new RecordingDtoRequest("", "","",
                "","","", "",
                "", "");

        MvcResult result = mvc.perform(post("/recording")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(recordingDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 400);
    }

    @Test
    public void testPostRecording4() throws Exception{
        RecordingDtoRequest recordingDtoRequest = new RecordingDtoRequest("Led zeppelin", "Black dog","Led zeppelin 4",
                "8-11-1971","./Cover/Led.png","Rock", "5.05",
                "./audio/black_dog.mp3", "./video/black_dog.png");

        MvcResult result = mvc.perform(post("/recording/12")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(recordingDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 404);
    }

}
