package app.endpoint;

import app.action.PromotionAction;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import request.DeletePromotionDtoRequest;
import request.PromotionDtoRequest;

import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = PromotionEndpoint.class)
public class PromotionEndpointTest {


    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper = new ObjectMapper();

    @MockBean
    private PromotionAction promotionAction;

    @Test
    public void promotionCampaign1() throws Exception {
        PromotionDtoRequest promotionDtoRequest = new PromotionDtoRequest(12, "start", ZonedDateTime.now());
        MvcResult result = mvc.perform(put("/promotion/12")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(promotionDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 200);
    }

    @Test
    public void promotionCampaign2() throws Exception {
        PromotionDtoRequest promotionDtoRequest = new PromotionDtoRequest(-1, "start", ZonedDateTime.now());
        MvcResult result = mvc.perform(put("/promotion/12")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(promotionDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 400);
    }

    @Test
    public void promotionCampaign3() throws Exception {
        PromotionDtoRequest promotionDtoRequest = new PromotionDtoRequest(12, "stop", ZonedDateTime.now());
        MvcResult result = mvc.perform(put("/promotion/12")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(promotionDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 200);
    }

    @Test
    public void promotionCampaign4() throws Exception {
        PromotionDtoRequest promotionDtoRequest = new PromotionDtoRequest(12, "start", ZonedDateTime.now());
        MvcResult result = mvc.perform(put("/promotion/-1")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(promotionDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 400);
    }

    @Test
    public void promotionDeleteCampaign1() throws Exception {
        DeletePromotionDtoRequest deletePromotionDtoRequest = new DeletePromotionDtoRequest(12, ZonedDateTime.now());
        MvcResult result = mvc.perform(delete("/promotion/12")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(deletePromotionDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 200);
    }

    @Test
    public void promotionDeleteCampaign2() throws Exception {
        DeletePromotionDtoRequest deletePromotionDtoRequest = new DeletePromotionDtoRequest(-1, ZonedDateTime.now());
        MvcResult result = mvc.perform(delete("/promotion/12")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(deletePromotionDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 400);
    }

    @Test
    public void promotionDeleteCampaign3() throws Exception {
        DeletePromotionDtoRequest deletePromotionDtoRequest = new DeletePromotionDtoRequest(12, ZonedDateTime.now());
        MvcResult result = mvc.perform(delete("/promotion/-1")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(deletePromotionDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 400);
    }

    @Test
    public void promotionDeleteCampaign4() throws Exception {
        DeletePromotionDtoRequest deletePromotionDtoRequest = new DeletePromotionDtoRequest(12);
        MvcResult result = mvc.perform(delete("/promotion/12")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(deletePromotionDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 200);
    }


}
