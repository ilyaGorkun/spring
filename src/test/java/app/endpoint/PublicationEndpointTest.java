package app.endpoint;

import app.action.ChannelAction;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import request.DeleteDtoRequest;
import request.PublicationDtoRequest;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = PublicationEndpoint.class)
public class PublicationEndpointTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper = new ObjectMapper();

    @MockBean
    private ChannelAction channelActionAction;

    @Test
    public void testPostInChanel1() throws Exception {
        PublicationDtoRequest publicationDtoRequest = new PublicationDtoRequest(12,"Itunes", ZonedDateTime.now());
        MvcResult result = mvc.perform(post("/publication/12")
                    .contentType(MediaType.APPLICATION_JSON )
                    .content(mapper.writeValueAsString(publicationDtoRequest)))
                    .andReturn();
        assertEquals(result.getResponse().getStatus(), 200);
    }

    @Test
    public void testPostInChanel2() throws Exception {
        PublicationDtoRequest publicationDtoRequest = new PublicationDtoRequest(-1,"Itunes", ZonedDateTime.now());
        MvcResult result = mvc.perform(post("/publication/12")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(publicationDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 400);
    }

    @Test
    public void testPostInChanel3() throws Exception {
        PublicationDtoRequest publicationDtoRequest = new PublicationDtoRequest(12,"", ZonedDateTime.now());
        MvcResult result = mvc.perform(post("/publication/12")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(publicationDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 400);
    }

    @Test
    public void testPostInChanel4() throws Exception {
        PublicationDtoRequest publicationDtoRequest = new PublicationDtoRequest(12,"Itunes", ZonedDateTime.now());
        MvcResult result = mvc.perform(post("/publication/-1")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(publicationDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 400);
    }

    @Test
    public void testDeleteInChanel1() throws Exception {
        DeleteDtoRequest deleteDtoRequest = new DeleteDtoRequest(12, "iTunes");
        MvcResult result = mvc.perform(delete("/publication/12")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(deleteDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 200);
    }

    @Test
    public void testDeleteInChanel2() throws Exception {
        DeleteDtoRequest deleteDtoRequest = new DeleteDtoRequest(-1, "iTunes");
        MvcResult result = mvc.perform(delete("/publication/12")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(deleteDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 400);
    }

    @Test
    public void testDeleteInChanel3() throws Exception {
        DeleteDtoRequest deleteDtoRequest = new DeleteDtoRequest(12, "");
        MvcResult result = mvc.perform(delete("/publication/12")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(deleteDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 400);
    }

    @Test
    public void testDeleteInChanel4() throws Exception {
        DeleteDtoRequest deleteDtoRequest = new DeleteDtoRequest(12, "iTunes");
        MvcResult result = mvc.perform(delete("/publication/-1")
                .contentType(MediaType.APPLICATION_JSON )
                .content(mapper.writeValueAsString(deleteDtoRequest)))
                .andReturn();
        assertEquals(result.getResponse().getStatus(), 400);
    }




}
